﻿Hallo dit is de test opdracht van Jelmer lam

Belangrijke Equivalentieklassen:
Postalcode check = 0456, 2500 , 3550, 3700, 4500
License age = 10-10-1000, 10-10-2015 , 10-10-2030
ConstructionYear = 1000, 2015 , 2030 

Testen die ik ga maken:
Test case 1 PolicyHolderPostalCodeIsCheckedAndCalculateRate:
Doel: Het doel van deze unit test is om te kijken of de applicatie de postcode check en dan het goeie tarief daarbij pakt.
Keuze data invulling: Ik heb er voor gekozen om 5 equivalentieklassen te checken dat zijn 0456, 2500 , 3550, 3700, 4500 dit heb ik gedaan
omdat 3 van de waardes buiten de range van postcode verhoging tarief vallen en de andere 2 erin om te kijken of die ook echt dan geen tarief erover doet.
Technieken:
Ik heb hier gebruik gemaakt van een theory zodat ik meerdere postcodes kon checken met 1 test case. Als eerste heb ik een vehicle instance gemaakt voor beide.
Daarna heb ik 2 policyholders instances gemaakt waarvan 1 standaard buiten de range valt van de postcode verhoging tarief. 
Dit zorgt ervoor dat ik zeker weet dat die geen tarief pak waardoor ik de normale berekening krijg.

De expected berekening krijg ik door een instance te  maken van de premiumcalculation en daarna maak ik handmatig de bereking van postal code waardoor de originele waarde 
van de policyholderbase postalcode wordt overschreven met de theory's die ik heb. daarna doe ik de premiumCalculationBase.PremiumAmountPerYear * de vermeningvuldige functie en dit zorgt
ervoor dat ik het goeie tarief van de postal code berekening krijg.

De machine berekening krijg ik door een instance te maken van de primiumcalculation en hier gewoon alle data aan meegeven daarna vraag ik in de assert
of die de  premiumCalculation.PremiumAmountPerYear kan pakken want dan hoort die ook al de calculatie te doen van de postal code tarief.

Test case 2 PremiumCalculationsCanCalculateTheBasePremium:
Doel: Het doel van deze text case is om te kijken of de applicatie de basis premie kan berekenen.
Keuze data invulling: Ik heb hier voor data gekozen waar geen extra requirements aan zitten zodat je de echte basis premie krijgt.
Technieken: ik heb gekozen om een fact te gebruiken omdat ik maar 1 dataset hoef te testen omdat ik alleen de basis premie wil.
Voor expected value heb ik de basis premie gepakt en deze handmatig met data gevuld zodat ik deze berekening test.
Daarna heb ik ik een instance gemaakt van vehicle en policyholder en deze meegeven aan de instance die ik daarna maakte van premiumcalculation.
en in de assert heb ik dan de premiumcalculation.PremiumAmountPerYear gezet want dan berekent die de waarde en deze vergeleken met de expected value.

Test case 3 PremiumCalculationsWaPlusRaiseTest:
Doel: Het doel van deze text case is om te kijken of de applicatie de WaPlus premie kan berekenen.
Keuze data invulling: Ik heb hier voor data gekozen waar geen extra requirements aan zitten zodat je de basis premie krijgt en daarop dan de opslag om Waplus te krijgen doe.
Technieken: ik heb gekozen om een fact te gebruiken omdat ik maar 1 dataset hoef te testen omdat ik alleen de Waplus premie wil.
als eerste heb ik een instance gemaakt van vehicle en policyholder zodat deze vast staan.
Voor expected value heb ik een instance gemaakt van premiumcalculation met vehicle en policyholder instance data erin voor de basis premie 
en deze handmatig in de assert * 1.2 doe waardoor je de Waplus premie krijgt.
Voor de echte calculatie maak ik weer een instance van premiumcalculation met vehicle en policholder instance data alleen hier geef ik de Waplus premie mee.
en in de assert heb ik dan de premiumcalculationWaPlus.PremiumAmountPerYear gezet want dan berekent die de waarde van WaPlus en deze vergeleken met de expected value.

Test case 4 PremiumCalculationAllRiskRaiseTest:
Doel: Het doel van deze text case is om te kijken of de applicatie de Allrisk premie kan berekenen.
Keuze data invulling: Ik heb hier voor data gekozen waar geen extra requirements aan zitten zodat je de basis premie krijgt en daarop dan de opslag om Allrisk te krijgen doe.
Technieken: ik heb gekozen om een fact te gebruiken omdat ik maar 1 dataset hoef te testen omdat ik alleen de Allrisk premie wil.
als eerste heb ik een instance gemaakt van vehicle en policyholder zodat deze vast staan.
Voor expected value heb ik een instance gemaakt van premiumcalculation met vehicle en policyholder instance data erin voor de basis premie 
en deze handmatig in de assert * 2 doe waardoor je de Allrisk premie krijgt.
Voor de echte calculatie maak ik weer een instance van premiumcalculation met vehicle en policholder instance data alleen hier geef ik de Allrisk premie mee.
en in de assert heb ik dan de premiumcalculationAllRisk.PremiumAmountPerYear gezet want dan berekent die de waarde van Allrisk en deze vergeleken met de expected value.

Test case 5  PremiumCalculationCanCalculateTheNoClaimYearsDiscountTest:
Doel: Het doel van deze text case is om te kijken of de applicatie de schadevrije jaren korting kan berekenen.
Keuze data invulling: Ik heb hier voor data gekozen waar geen extra requirements aan zitten behalve de schadevrije jaren zodat ik alleen met die korting rekening hoef te houden.
Technieken: ik heb gekozen om een fact te gebruiken omdat ik maar 1 dataset wil testen omdat als die de korting bij 7 schadevrije jaren goed doet ik ook kan verwachten dat de rest het goed doet.
als eerste heb ik een instance gemaakt van vehicle zodat deze vast staat.
Daarna maak ik de eerste policyholder aan die voor de expected value. deze policholder en de vehicle instance stop ik dan in een instance van premiumcalculation
en hier geef ik ook de basispremie mee zodat ik ook geen last heb van de optellingen van de andere premie's. deze premiumcalculation stop ik dan in een nieuwe
variable waar ik dan vraag om de premium amount per year en deze doe ik dan handmatig de bereking over van de no claim years discount.
Voor de echte calculatie maak ik een nieuwe policyholder instance aan en deze gooi ik dan in een nieuwe instance van premiumcalculation waarna ik dan de policyholderone
calculation vergelijk met de nieuwe premiumcalculation.PremiumAmountPerYear.

Test case 6 PremiumCalculationsCanCalculateTheMonthPrice:
Doel: Het doel van deze text case is om te kijken of de applicatie de prijs kan berekenen als je maandelijks betaald.
Keuze data invulling: Ik heb hier voor data gekozen waar geen extra requirements aan zitten zodat ik alleen de jaarlijkse betaling korting rekening mee hoef te houden.
Technieken: ik heb gekozen om een fact te gebruiken omdat ik maar 1 dataset wil testen namelijk of die de maandelijkse prijs goed berekent.
als eerste heb ik een instance gemaakt van vehicle en policyholder zodat deze vast staan.
Daarna heb ik een instance gemaakt van premiumcalculation en deze gemocked om te kijken hoe het werkt en om de maandelijkse calculation vast te zetten zodat de onderhoudbaarheid
van de test beter word. hier voegte ik de vehicle en policyholder instance data erin met de basispremie.
Voor de expected value pak ik dan een premiumcalculation instance en die stop ik in een nieuwe variable waar ik de berekening die de computer doet naboost.
Voor deze berekening zet ik Math.Round want deze functie wordt ook gebruikt bij de computer berekening en anders krijg je nooit een groene test terug.
Dan in de assert zet ik de expected value en vergelijk deze met de instance van premiumcalculation en ga dan de methods door tot dat die de paymentperiod per maand heeft.


Test case 7 PremiumCalculationsCanCalculateTheYearPrice:
Doel: Het doel van deze text case is om te kijken of de applicatie de prijs kan berekenen als je jaarlijks betaald.
Keuze data invulling: Ik heb hier voor data gekozen waar geen extra requirements aan zitten zodat ik alleen de jaarlijkse betaling krijg zonder rekening te houden met de rest.
Technieken: ik heb gekozen om een fact te gebruiken omdat ik maar 1 dataset wil testen namelijk of die de jaarlijkse prijs goed berekent.
als eerste heb ik een instance gemaakt van vehicle en policyholder zodat deze vast staan.
Daarna heb ik een instance gemaakt van premiumcalculation met vehicle en policyholder instance data erin samen met de basispremie. zodat ik deze voor beide berekeningen
Kan gebruiken. Voor de expected value pak ik dan deze premiumcalculation en die stop ik in een nieuwe variable waar ik de berekening die de computer doet naboost.
Voor deze berekening zet ik Math.Round want deze functie wordt ook gebruikt bij de computer berekening en anders krijg je nooit een groene test terug.
Dan in de assert zet ik de expected value en vergelijk deze met de instance van premiumcalculation en ga dan de methods door tot dat die de paymentperiod per jaar heeft.



Test case 8 PolicyHolderLicenseAgeIsCheckedAndIsAccepted:
Doel: Het doel van deze unit test is om te kijken of de applicatie de policyholder license age checkt en dan de age berekent.
Keuze data invulling: Ik heb er voor gekozen om 3 equivalentieklassen te checken dat zijn 10-10-1000, 10-10-2015 , 10-10-2030 dit heb ik gedaan
omdat 2 van de waardes buiten de range van mogelijke license age vallen en de andere valt om te kijken of die 2 ook accepteerd want dat is dan een fout.
Technieken:
Ik heb hier gebruik gemaakt van een theory zodat ik meerdere license ages kon checken met 1 test case. 
Als eerste heb ik een policyholder instance gemaakt voor de computer berekening.
De actuel value heb ik gekregen  door de policyholder license age op te vragen van de computer.

De expected value heb ik gekregen door het in de inline data te zetten van de theory wat ik verwacht dat het hoort te zijn.
Deze vergelijk ik dan met elkaar in de assert.


Test case 9 VehicleAgeIsCalculatedCorrectlyl:
Doel: Het doel van deze unit test is om te kijken of de applicatie de vehicle age checked en dan de vehicle age berekent.
Keuze data invulling: Ik heb er voor gekozen om 3 equivalentieklassen te checken dat zijn 1000, 2015 , 2030 dit heb ik gedaan
omdat 2 van de waardes buiten de range van mogelijke bouwjaren van vehicle age vallen en de andere valt om te kijken of die 2 ook accepteerd want dat is dan een fout.
Technieken:
Ik heb hier gebruik gemaakt van een theory zodat ik meerdere vehicle ages kon checken met 1 test case. 
Als eerste heb ik een vehicle instance gemaakt voor de computer berekening.
De actuel value heb ik gekregen  door de vehicle age op te vragen van de computer.

De expected value heb ik gekregen door het in de inline data te zetten van de theory wat ik verwacht dat het hoort te zijn.
Deze vergelijk ik dan met elkaar in de assert.


Fouten gevonden in appicatie:

Algemene fouten:
Getest: invoer door user met de tegenoverstellende primitive types
verwacht: dat die dit niet accepteerd en dat die zegt dat je het opnieuw moet invoeren
Waarom deze verwachting: Omdat het best logisch is om op een plek waar je een int in moet voeren je geen string kan invoeren en hij je het opnieuw vraagt
Oplossing: Ik heb er een gemaakt voor de age input waar die door een loop blijft gaan als je niet het goeie antwoord terug geeft.

1. Premiumcalculation fouten:

Getest: UpdatePremiumForNoClaimYears of die het goed doorgeeft
verwacht: dat die een double meegeeft voor verdere berekening
Waarom deze verwachting: Omdat er ook in de parameters staat dat het een double is en die gebruikt voor verdere berekeningen
oplossing: er handmatig een dubbel van maken in de code zodat die het goed meegeeft

Getest: PremiumPaymentAmount getest of die goed werkt
verwacht: dit die goed werkt
Waarom deze verwachting: omdat je verwacht dat de applicatie goed weet hoe die een premie moet berekenen
oplossing: 1000*1 - 1000 * 0.025  = 1000 * (1 - 0.025) =  0.975

Getest: CalculateBasePremium of die het goed doorgeeft
verwacht: dat die een double meegeeft voor verdere berekening
Waarom deze verwachting: Omdat de berekingen doormiddel van een double wordt berekent en dit dan weer doorgeven moet worden
oplossing: //return vehicle.ValueInEuros / 100 - vehicle.Age + vehicle.PowerInKw / 5 / 3;
return ((double)vehicle.ValueInEuros / 100 - vehicle.Age + (double)vehicle.PowerInKw / 5 )/ 3;

2. Vehicle fouten:

Getest: Een auto kan gebouwd zijn in de toekomst
verwacht: dat die dit niet accepteerd word
Waarom deze verwachting: omdat je nog geen verzekering kan afsluiten voor auto's die in de toekomst gebouwd zijn

3. PolicyHolder fouten:

Getest: Je kan je rijbewijs 200 jaar geleden gehaald hebben
verwacht: dat die dit niet accepteerd
Waarom deze verwachting: omdat geen een mens 200 jaar oud kunnen zijn

Getest: Policy holders age kan onder de 18 zijn 
verwacht: dat die dit niet accepteerd
Waarom deze verwachting: omdat je op die leeftijd nog geen rijbewijs hoort te hebben dus ook geen verzekering hoort te kunnen afsluiten

Getest: Je kan in de toekomst je rijbewijs krijgen
verwacht: dat die dit niet accepteerd
Waarom deze verwachting: Omdat je nog geen rijbewijs hebt eigenlijk dus ook nog geen verzekering hoort te kunnen afsluiten








