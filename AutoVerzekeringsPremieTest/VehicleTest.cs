﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace AutoVerzekeringsPremieTest
{
    public class VehicleTest
    {
        [Theory]
        [InlineData(250, 3000, 1000, 1021)]
        [InlineData(190, 4000, 2015, 6)]
        [InlineData(210, 4500, 2030, 0)]
        public void VehicleAgeIsCalculatedCorrectly(int PowerInKw, int ValueInEuros, int constructionYear, int expectedValue)
        {
            // Arrange

            var vehicle = new Vehicle(PowerInKw, ValueInEuros, constructionYear);

            // Act
            var actualValue = vehicle.Age;

            //Assert
            Assert.Equal(expectedValue, actualValue);
        }
    }
}
