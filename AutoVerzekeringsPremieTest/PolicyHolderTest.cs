﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace AutoVerzekeringsPremieTest
{
    public class PolicyHolderTest

    {
        [Theory]
        [InlineData(25, "10-10-2015", 1920, 4, 5)]
        [InlineData(19, "10-10-1000", 1930, 100, 1020)]
        [InlineData(21, "10-10-2030", 1940, 20, -10)]
        public void PolicyHolderLicenseAgeIsCheckedAndIsAccepted(int Age, string DriverlicenseStartDate, int PostalCode, int NoClaimYears, int expectedValue)
        {
            // Arrange

            var policyholder = new PolicyHolder(Age, DriverlicenseStartDate, PostalCode, NoClaimYears);

            // Act
            var actualValue = policyholder.LicenseAge;

            //Assert
            Assert.Equal(expectedValue, actualValue);

        }
    }
}
